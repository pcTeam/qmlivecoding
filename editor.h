#ifndef EDITOR_H
#define EDITOR_H

#include <QTextEdit>
#include <QTextLine>

class Editor : public QTextEdit
{
    Q_OBJECT
public:
    explicit Editor(QWidget *parent = 0);
    
protected:
    void keyReleaseEvent(QKeyEvent *e);
    void keyPressEvent(QKeyEvent *e);

private:
    QString tabs() const;
    int currentTextLine() const;

};

#endif // EDITOR_H
