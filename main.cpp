#include <QMenuBar>
#include <QApplication>
#include <QtDeclarative>
#include "codeeditor.h"
#include "project.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    QWidget* topWindow = new QWidget();
    if (QApplication::desktop())
        topWindow->resize(QApplication::desktop()->screen()->size());

    CodeEditor* codeEditor = new CodeEditor();
    Project* project = new Project();

    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(&codeEditor->window);
    layout->addWidget(&project->viewer);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    topWindow->setLayout(layout);

    QMenuBar menu(topWindow);
    QAction* develop = menu.addAction("Develop");
    QAction* run = menu.addAction("Run");
    QAction* load = menu.addAction("Import");
    QAction* save = menu.addAction("Export");
    menu.addAction("Exit", app.data(), SLOT(quit()));       

    QObject::connect(develop, SIGNAL(triggered()), project, SLOT(clean()));
    QObject::connect(develop, SIGNAL(triggered()), &project->viewer, SLOT(hide()));
    QObject::connect(develop, SIGNAL(triggered()), &codeEditor->window, SLOT(show()));
    QObject::connect(develop, SIGNAL(triggered()), &codeEditor->window, SLOT(setFocus()));

    QObject::connect(run, SIGNAL(triggered()), &codeEditor->window, SLOT(hide()));
    QObject::connect(run, SIGNAL(triggered()), &project->viewer, SLOT(show()));
    QObject::connect(run, SIGNAL(triggered()), &project->viewer, SLOT(setFocus()));
    QObject::connect(run, SIGNAL(triggered()), project, SLOT(run()));

    QObject::connect(load, SIGNAL(triggered()), develop, SLOT(trigger()));
    QObject::connect(load, SIGNAL(triggered()), codeEditor, SLOT(importFile()));

    QObject::connect(save, SIGNAL(triggered()), develop, SLOT(trigger()));
    QObject::connect(save, SIGNAL(triggered()), codeEditor, SLOT(exportFile()));

    codeEditor->load();
    topWindow->show();
    develop->trigger();    

    return app->exec();
}
