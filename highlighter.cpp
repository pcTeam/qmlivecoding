#include "highlighter.h"
#include "highlightingRules/types.h"
#include "highlightingRules/items.h"
#include "highlightingRules/bindings.h"
#include "highlightingRules/keywords.h"
#include "highlightingRules/modules.h"
#include "highlightingRules/signals.h"
#include "highlightingRules/colors.h"

Highlighter::Highlighter(QTextDocument *parent) : QSyntaxHighlighter(parent)
{
    prepareLiteralRules(QColor::fromRgb(255, 85, 255));    
    prepareHighlightingRules(types, QColor::fromRgb(255, 255, 85));
    prepareHighlightingRules(items, Qt::green);
    prepareHighlightingRules(bindings, QColor::fromRgb(255, 85, 85));
    prepareHighlightingRules(keywords, QColor::fromRgb(255, 255, 85));
    prepareHighlightingRules(modules, Qt::blue);
    prepareHighlightingRules(_signals, QColor::fromRgb(255, 255, 85));
    prepareColorRules(colors);
    prepareCommentRules(QColor::fromRgb(85, 255, 255));
}

void Highlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegExp expression(rule.pattern);
        int index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(expression.pos(1), expression.capturedTexts().at(1).length(), rule.format);
            index = expression.indexIn(text, index + length);
        }
    }

    QTextCharFormat format;
    format.setUnderlineStyle(QTextCharFormat::SingleUnderline);
    format.setForeground(QColor::fromRgb(255, 85, 255));
    QRegExp expression("color:\\s*\"(#[0-9a-z]{6,6})\"");
    expression.setCaseSensitivity(Qt::CaseInsensitive);
    int index = expression.indexIn(text);
    while (index >= 0) {
        format.setUnderlineColor(QColor(expression.capturedTexts().at(1)));
        int length = expression.matchedLength();
        setFormat(expression.pos(1), expression.capturedTexts().at(1).length(), format);
        index = expression.indexIn(text, index + length);
    }
}

void Highlighter::prepareHighlightingRules(const char **words, const QColor &color)
{
    QTextCharFormat format;
    format.setForeground(color);

    HighlightingRule rule;

    const char** current = words;
    while (*current)
    {
        rule.pattern = QRegExp(QString("\\b(%1)\\b").arg(*current));
        rule.format = format;
        highlightingRules << rule;
        current++;
    }
}

void Highlighter::prepareColorRules(const char **words)
{
    QTextCharFormat format;
    format.setForeground(QColor::fromRgb(255, 85, 255));
    format.setUnderlineStyle(QTextCharFormat::SingleUnderline);

    HighlightingRule rule;

    const char** current = words;
    while (*current)
    {
        format.setUnderlineColor(QColor(*current));
        rule.pattern = QRegExp(QString("color:\\s*\"(%1)\"").arg(*current));
        rule.pattern.setCaseSensitivity(Qt::CaseInsensitive);
        rule.format = format;
        highlightingRules << rule;
        current++;
    }
}

void Highlighter::prepareLiteralRules(const QColor& color)
{
    QTextCharFormat format;
    format.setForeground(color);

    HighlightingRule rule;

    rule.pattern = QRegExp("(\".*\")");
    rule.format = format;
    highlightingRules << rule;

    rule.pattern = QRegExp("(\\d+\\.?\\d+)");
    highlightingRules << rule;

    rule.pattern = QRegExp("(\\d+)");
    highlightingRules << rule;

}

void Highlighter::prepareCommentRules(const QColor &color)
{
    QTextCharFormat format;
    format.setForeground(color);

    HighlightingRule rule;

    rule.pattern = QRegExp("(//.*)$");
    rule.format = format;
    highlightingRules << rule;

    rule.pattern = QRegExp("(/\\*.*\\*/)");
    highlightingRules << rule;
}
