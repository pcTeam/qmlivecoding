#include <QDebug>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QApplication>
#include <QTextEdit>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include "codeeditor.h"
#include "qmlapplicationviewer.h"

CodeEditor::CodeEditor(QWidget *parent) : QObject(parent), window(parent), textEditor(), highlighter(textEditor.document())
{
    completer = new Completer();
    filterModel = new QSortFilterProxyModel(completer);    

    QPalette Pal(textEditor.palette());    
    Pal.setColor(QPalette::Base, Qt::black);
    Pal.setColor(QPalette::Foreground, QColor("#AAAAAA"));
    Pal.setColor(QPalette::Text, QColor("#AAAAAA"));
    completer->setPalette(Pal);
    textEditor.setPalette(Pal);

    filterModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    filterModel->setSourceModel(&completionModel);
    completer->setModel(filterModel);
    textEditor.setWordWrapMode(QTextOption::NoWrap);
    textEditor.setReadOnly(false);
    QFont font("Droid Sans Mono", 10);
    textEditor.setFont(font);
    // BUG: something strange here
    textEditor.setTabStopWidth(QFontMetrics(font).boundingRect(QString("  ")).width() * 2);
    connect(&textEditor, SIGNAL(textChanged()), this, SLOT(save()));
    connect(&textEditor, SIGNAL(textChanged()), this, SLOT(updateCompleter()));
    connect(completer, SIGNAL(selected(QString)), this, SLOT(insertSnippet(QString)));

    completer->hideCompleter();
    layout.setSpacing(1);
    layout.setContentsMargins(0, 0, 0, 0);
    layout.addWidget(&textEditor);
    layout.addWidget(completer);
    window.setLayout(&layout);
}

void CodeEditor::load()
{
    if (!QFile::exists("/data/data/org.PC.QMLiveCoding/main.qml") && QFile("assets:/qml/QMLiveCoding/sample.qml").exists())
    {
        QFile source("assets:/qml/QMLiveCoding/sample.qml");

        QFile dest("/data/data/org.PC.QMLiveCoding/main.qml");
        if (dest.open(QIODevice::WriteOnly | QIODevice::Truncate) && source.open(QIODevice::ReadOnly))
        {
            dest.write(source.readAll());
        }
    }

    QFile file("/data/data/org.PC.QMLiveCoding/main.qml");
    if (file.open(QIODevice::ReadOnly))
    {
        QString text = file.readAll();
        file.close();
        textEditor.setText(text);
    }
}

void CodeEditor::save()
{
    QFile file("/data/data/org.PC.QMLiveCoding/main.qml");
    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream stream(&file);
        stream << textEditor.toPlainText();
    }
    else
    {
        qDebug() << file.errorString();
    }
}

void CodeEditor::exportFile()
{
    QString fileName = QFileDialog::getSaveFileName(&this->textEditor, "Save QML file...", "/mnt/sdcard", QString(), 0, QFileDialog::DontConfirmOverwrite);
    if (!fileName.isEmpty())
    {
        QFile source("/data/data/org.PC.QMLiveCoding/main.qml");

        QFile dest(fileName);
        if (dest.open(QIODevice::WriteOnly | QIODevice::Truncate) && source.open(QIODevice::ReadOnly))
        {
            dest.write(source.readAll());
        }
    }
}

void CodeEditor::importFile()
{
    QString fileName = QFileDialog::getOpenFileName(&this->textEditor, "Open QML file...", "/mnt/sdcard");
    if (!fileName.isEmpty())
    {
        QFile source(fileName);

        QFile dest("/data/data/org.PC.QMLiveCoding/main.qml");
        if (dest.open(QIODevice::WriteOnly | QIODevice::Truncate) && source.open(QIODevice::ReadOnly))
        {
            dest.write(source.readAll());
            dest.close();
        }

        load();
    }
}

void CodeEditor::updateCompleter()
{
    int cursorPosition = textEditor.textCursor().positionInBlock() - 1;
    if (cursorPosition < 0)
    {
        completer->hideCompleter();
        return;
    }

    QString word;

    QString text = textEditor.textCursor().block().text();
    while (cursorPosition >= 0 && text[cursorPosition].isLetter())
    {
        word.prepend(text[cursorPosition]);
        cursorPosition--;
    }

    if (word.isEmpty())
    {
        filterModel->setFilterFixedString(QString());
        completer->hideCompleter();
    }
    else
    {
        filterModel->setFilterRegExp(QString("^") + word + ".*");
        completer->activateCompleter();
    }
}

void CodeEditor::insertSnippet(const QString &snippet)
{
    int cursorPosition = textEditor.textCursor().positionInBlock() - 1;

    int count = 0;

    QString text = textEditor.textCursor().block().text();
    while (cursorPosition >= 0 && text[cursorPosition].isLetter())
    {
        cursorPosition--;
        count++;
    }

    while (count-- > 0)
        textEditor.textCursor().deletePreviousChar();

    if (snippet.contains("\n"))
    {
        QStringList lines = snippet.split("\n", QString::SkipEmptyParts);
        for(int i = 0; i < lines.count(); i++)
        {
            if (lines[i] == "}")
            {
                int position = textEditor.textCursor().positionInBlock();
                QString text = textEditor.textCursor().block().text();
                if (position > 0 && text[position - 1] == '\t')
                    textEditor.textCursor().deletePreviousChar();
            }

            textEditor.insertPlainText(lines[i]);

            if (i != lines.count() - 1)
            {
                QKeyEvent key_press(QKeyEvent::KeyPress, Qt::Key_Enter, 0);
                QApplication::sendEvent(&textEditor, &key_press);

                QKeyEvent key_release(QKeyEvent::KeyRelease, Qt::Key_Enter, 0);
                QApplication::sendEvent(&textEditor, &key_release);
            }
        }
    }
    else
    {
        textEditor.insertPlainText(snippet);
    }

    cursorPosition = textEditor.textCursor().position() - 1;
    text = textEditor.toPlainText();
    while (cursorPosition >= 0 && text[cursorPosition] != '$')
    {
        cursorPosition--;
    }
    QTextCursor cursor = textEditor.textCursor();
    cursor.setPosition(cursorPosition + 1);
    textEditor.setTextCursor(cursor);
    if (cursorPosition >= 0 && text[cursorPosition] == '$')
        textEditor.textCursor().deletePreviousChar();

    completer->hideCompleter();
}
