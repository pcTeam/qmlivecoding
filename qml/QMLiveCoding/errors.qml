import QtQuick 1.1

Rectangle {
    width: 100
    height: 62

    ListView {
        delegate: Row {
            Text { text: modelData }
        }

        anchors.fill: parent
        model: errorsModel
    }
}
