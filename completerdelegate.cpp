#include <QApplication>
#include <QPainter>
#include "completerdelegate.h"

CompleterDelegate::CompleterDelegate(QObject *parent) : QAbstractItemDelegate(parent)
{
}

void CompleterDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{    
    if (index.column() == 0)
    {
        QString text = index.data().toString();

        painter->setPen(index.data(Qt::TextColorRole).value<QColor>());
        painter->drawText(option.rect, text);
    }
}

QSize CompleterDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    return QSize(300, 50);
}
