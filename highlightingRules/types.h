#ifndef TYPES_H
#define TYPES_H

static const char* types[] = {
    "int", "bool", "real", "double", "string", "url", "list", "var", "enumeration", "color", "font", "matrix4x4",
    "quaternion", "vector2d", "vector3d", "vector4d", "date", "time", "point", "size", "rect",
    NULL
};

#endif // TYPES_H
